package authentication.controller;

import com.sun.jna.platform.win32.Sspi;
import com.sun.jna.platform.win32.SspiUtil;
import authentication.om.AuthenticationKerberosResponse;
import com.sun.jna.platform.win32.Win32Exception;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import waffle.windows.auth.IWindowsCredentialsHandle;
import waffle.windows.auth.IWindowsSecurityContext;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;
import waffle.windows.auth.impl.WindowsCredentialsHandleImpl;
import waffle.windows.auth.impl.WindowsSecurityContextImpl;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

@RestController
@RequestMapping(path="/kerberosAuthentication")
public class AuthenticateKerberosController {

    public static final String PRINCIPAL_NAME_KEY = "principalName";
    public static final String SECURITY_PACKAGE_TYPE = "Negotiate";

    @GetMapping("/getTicket")
    public ResponseEntity<?> getTicket(HttpServletRequest request) throws UnknownHostException {

        String principalName = request.getHeader(PRINCIPAL_NAME_KEY);
        String clientConnectionId = request.getRemoteAddr();
        String serverConnectionId = String.valueOf(InetAddress.getLocalHost());
        IWindowsSecurityContext serverContext = null;
        IWindowsCredentialsHandle clientCredentials;
        clientCredentials = new WindowsCredentialsHandleImpl(principalName,2, SECURITY_PACKAGE_TYPE);
        clientCredentials.initialize();
        WindowsSecurityContextImpl clientContext;
        // initial client security context
        try{
        clientContext = new WindowsSecurityContextImpl();
        clientContext.setPrincipalName(principalName);
        clientContext.setCredentialsHandle(clientCredentials);
        clientContext.setSecurityPackage(SECURITY_PACKAGE_TYPE);
        clientContext.initialize(null, null, principalName);
        // create an auth provider and a security context for the client on the server
        WindowsAuthProviderImpl provider = new WindowsAuthProviderImpl();
        // send the byte[] token to the server and the server will
        // response with another byte[] token, which the client needs to answer again
            do {
                // Step 2: If has been already build an initial security context for the client
                // on the server, send a token back to the client, which the client needs to
                // accept and send back to the server again (a handshake)
                if (Objects.nonNull(serverContext)) {
                    byte[] tokenForTheClientOnTheServer = serverContext.getToken();
                    SspiUtil.ManagedSecBufferDesc continueToken = new SspiUtil.ManagedSecBufferDesc(Sspi.SECBUFFER_TOKEN, tokenForTheClientOnTheServer);
                    clientContext.initialize(clientContext.getHandle(), continueToken, clientConnectionId);
                }
                // Step 1: accept the token on the server and build a security context
                // representing the client on the server
                byte[] tokenForTheServerOnTheClient = clientContext.getToken();
                serverContext = provider.acceptSecurityToken(serverConnectionId, tokenForTheServerOnTheClient, SECURITY_PACKAGE_TYPE);
            } while (clientContext.isContinue() || (Objects.nonNull(serverContext) && serverContext.isContinue()));
        } catch (Win32Exception exception){
                return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        // at the end of this handshake, we know on the server side who the
        // client is, only by exchanging byte[] arrays

        return new ResponseEntity<AuthenticationKerberosResponse>(new AuthenticationKerberosResponse(clientContext.getToken()), HttpStatus.OK);

    }
}
