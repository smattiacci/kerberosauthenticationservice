package authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthenticationKerberosApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationKerberosApplication.class, args);
	}

}
