package authentication.om;

public class AuthenticationKerberosResponse {

    private byte[] ticket;

    public AuthenticationKerberosResponse(byte[] ticket) {
        this.ticket = ticket;
    }

    public byte[] getTicket() {
        return ticket;
    }

    public void setTicket(byte[] ticket) {
        this.ticket = ticket;
    }
}
